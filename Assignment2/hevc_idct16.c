#include <immintrin.h>
#include <malloc.h>
#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <time.h>
#include <math.h>
#include <smmintrin.h>
#include <emmintrin.h>

#define IDCT_SIZE         16
#define ITERATIONS        1000000
#define MAX_NEG_CROP      1024

#define MIN(X,Y) ((X) < (Y) ? (X) : (Y))
#define MAX(X,Y) ((X) > (Y) ? (X) : (Y))

static const short g_aiT16[16][16] =
{
  { 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64, 64},
  { 90, 87, 80, 70, 57, 43, 25,  9, -9,-25,-43,-57,-70,-80,-87,-90},
  { 89, 75, 50, 18,-18,-50,-75,-89,-89,-75,-50,-18, 18, 50, 75, 89},
  { 87, 57,  9,-43,-80,-90,-70,-25, 25, 70, 90, 80, 43, -9,-57,-87},
  { 83, 36,-36,-83,-83,-36, 36, 83, 83, 36,-36,-83,-83,-36, 36, 83},
  { 80,  9,-70,-87,-25, 57, 90, 43,-43,-90,-57, 25, 87, 70, -9,-80},
  { 75,-18,-89,-50, 50, 89, 18,-75,-75, 18, 89, 50,-50,-89,-18, 75},
  { 70,-43,-87,  9, 90, 25,-80,-57, 57, 80,-25,-90, -9, 87, 43,-70},
  { 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64, 64,-64,-64, 64},
  { 57,-80,-25, 90, -9,-87, 43, 70,-70,-43, 87,  9,-90, 25, 80,-57},
  { 50,-89, 18, 75,-75,-18, 89,-50,-50, 89,-18,-75, 75, 18,-89, 50},
  { 43,-90, 57, 25,-87, 70,  9,-80, 80, -9,-70, 87,-25,-57, 90,-43},
  { 36,-83, 83,-36,-36, 83,-83, 36, 36,-83, 83,-36,-36, 83,-83, 36},
  { 25,-70, 90,-80, 43,  9,-57, 87,-87, 57, -9,-43, 80,-90, 70,-25},
  { 18,-50, 75,-89, 89,-75, 50,-18,-18, 50,-75, 89,-89, 75,-50, 18},
  {  9,-25, 43,-57, 70,-80, 87,-90, 90,-87, 80,-70, 57,-43, 25, -9}
};

static int64_t diff(struct timespec start, struct timespec end)
{
    struct timespec temp;
    int64_t d;
    if ((end.tv_nsec-start.tv_nsec)<0) {
        temp.tv_sec = end.tv_sec-start.tv_sec-1;
        temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
    } else {
        temp.tv_sec = end.tv_sec-start.tv_sec;
        temp.tv_nsec = end.tv_nsec-start.tv_nsec;
    }
    d = temp.tv_sec*1000000000+temp.tv_nsec;
    return d;
}

static void compare_results(short *ref, short *res, const char *msg)
{
    int correct =1;

    printf("Comparing %s\n",msg);
    for(int j=0; j<IDCT_SIZE; j++)  {
        for(int i=0; i<IDCT_SIZE; i++){
            if(ref[j*IDCT_SIZE+i] != res[j*IDCT_SIZE+i]){
                correct=0;
                printf("failed at %d,%d\t ref=%d, res=%d\n ", i, j, ref[j*IDCT_SIZE+i],res[j*IDCT_SIZE+i]);
            }
        }
    }
    if (correct){
        printf("correct\n\n");
    }
}

// this function is for timing, do not change anything here
static void benchmark( void (*idct16)(short *, short *), short *input, short *output, const char *version )
{
    struct timespec start, end;
    clock_gettime(CLOCK_REALTIME,&start);

    for(int i=0;i<ITERATIONS;i++)
        idct16(input, output);

    clock_gettime(CLOCK_REALTIME,&end);
    double avg = (double) diff(start,end)/ITERATIONS;
    printf("%10s:\t %.3f ns\n", version, avg);
}

//scalar code for the inverse transform
static void partialButterflyInverse16(short *src, short *dst, int shift)
{
  int E[8],O[8];
  int EE[4],EO[4];
  int EEE[2],EEO[2];
  int add = 1<<(shift-1);

  for (int j=0; j<16; j++)
  {
    /* Utilizing symmetry properties to the maximum to minimize the number of multiplications */
    for (int k=0; k<8; k++)
    {
      O[k] = g_aiT16[ 1][k]*src[ 16] + g_aiT16[ 3][k]*src[ 3*16] + g_aiT16[ 5][k]*src[ 5*16] + g_aiT16[ 7][k]*src[ 7*16] +
        g_aiT16[ 9][k]*src[ 9*16] + g_aiT16[11][k]*src[11*16] + g_aiT16[13][k]*src[13*16] + g_aiT16[15][k]*src[15*16];
    }
    for (int k=0; k<4; k++)
    {
      EO[k] = g_aiT16[ 2][k]*src[ 2*16] + g_aiT16[ 6][k]*src[ 6*16] + g_aiT16[10][k]*src[10*16] + g_aiT16[14][k]*src[14*16];
    }
    EEO[0] = g_aiT16[4][0]*src[ 4*16 ] + g_aiT16[12][0]*src[ 12*16 ];
    EEE[0] = g_aiT16[0][0]*src[ 0    ] + g_aiT16[ 8][0]*src[  8*16 ];
    EEO[1] = g_aiT16[4][1]*src[ 4*16 ] + g_aiT16[12][1]*src[ 12*16 ];
    EEE[1] = g_aiT16[0][1]*src[ 0    ] + g_aiT16[ 8][1]*src[  8*16 ];

    /* Combining even and odd terms at each hierarchy levels to calculate the final spatial domain vector */
    for (int k=0; k<2; k++)
    {
      EE[k] = EEE[k] + EEO[k];
      EE[k+2] = EEE[1-k] - EEO[1-k];
    }
    for (int k=0; k<4; k++)
    {
      E[k] = EE[k] + EO[k];
      E[k+4] = EE[3-k] - EO[3-k];
    }
    for (int k=0; k<8; k++)
    {
      dst[k]   = MAX( -32768, MIN( 32767, (E[k]   + O[k]   + add)>>shift ));
      dst[k+8] = MAX( -32768, MIN( 32767, (E[7-k] - O[7-k] + add)>>shift ));
    }
    src ++;
    dst += 16;
  }
}

static void partialButterflyInverse16_simd(short *src, short *dst, int shift)
{
  int E[8],O[8], T[8] __attribute__ ( ( aligned ( 32 ) ) );
  int EE[4],EO[4] __attribute__ ( ( aligned ( 32 ) ) );
  int EEO[4]__attribute__((aligned(16))); ;//,EEO[2];
  int add = 1<<(shift-1);

  int alighn_data0 [64] __attribute__ ((aligned(32)));
  __m128i *alighn_data0_vec = (__m128i*) alighn_data0;
  int alighn_data1 [16] __attribute__ ((aligned(32)));
  __m128i *alighn_data1_vec = (__m128i*) alighn_data1;
  int out[4] __attribute__ ((aligned(32)));
   __m128i *out_vec = (__m128i*) out;
   //int *O_v = (int *) O;


	short mulData[16]__attribute__((aligned(16)));



  __m128i *g_aiT16_vec = (__m128i*) g_aiT16;
  __m128i *source = (__m128i*) src;
	__m128i *dest = (__m128i*) dst;
	__m128i *gAiT16 = (__m128i*) g_aiT16;
	__m128i *elReg = (__m128i*) E;
	__m128i *ehReg = (__m128i*) &E[4];
	__m128i *OReg = (__m128i*) O;
	__m128i *eeReg = (__m128i*) EE;
	__m128i *eoReg = (__m128i*) EO;
	__m128i *eeeReg = (__m128i*) &EEO[2];
	__m128i *eeoReg = (__m128i*) EEO;
	__m128i *mul0 = (__m128i*) mulData;
	__m128i *mul1 = (__m128i*) &mulData[8];
   //  _mm_store_si128(&test_vec[0],g_aiT16_vec[2]);

  //SIMD Registers to hold data
  __m128i eee;
  __m128i eeo;
  __m128i ee;
  __m128i eo;
  __m128i el;
  __m128i eh;
  __m128i temp;
  __m128i temp2;
  __m128i mul16Low;
  __m128i mul16high;
  __m128i mulTemp;
  __m128i mulTemp1;
  __m128i addReg;
  


for (int j=0; j<16; j++)
  {
    /* Utilizing symmetry properties to the maximum to minimize the number of multiplications */
    for (int k=1; k<16; k+=2){
      // load the inputs
      __m128i I0 = _mm_load_si128(&g_aiT16_vec[((k)*2)]);
      __m128i I1 = _mm_set1_epi16(src[16*(k)]);
      // all multiplications
      __m128i I2 = _mm_mullo_epi16(I1, I0);
      __m128i I3 = _mm_mulhi_epi16(I1, I0);
      __m128i I4 = _mm_unpacklo_epi16(I2, I3);
      __m128i I5 = _mm_unpackhi_epi16(I2, I3);
      _mm_store_si128(&alighn_data0_vec[k-1],I4);
      _mm_store_si128(&alighn_data0_vec[k],I5);
    }
//   printf("%d, %d \n",alighn_data0[16], g_aiT16[5][0]*src[5*16]);

    for (int k=0; k<8; k++)
    {
  //   T[k] = g_aiT16[ 1][k]*src[ 16] + g_aiT16[ 3][k]*src[ 3*16] + g_aiT16[ 5][k]*src[ 5*16] + g_aiT16[ 7][k]*src[ 7*16] + g_aiT16[ 9][k]*src[ 9*16] + g_aiT16[11][k]*src[11*16] + g_aiT16[13][k]*src[13*16] + g_aiT16[15][k]*src[15*16];

      __m128i I6 = _mm_set_epi32(alighn_data0[(k)],alighn_data0[(k+8)],alighn_data0[(k+16)],alighn_data0[(k+24)]);
      __m128i I7 = _mm_set_epi32(alighn_data0[(k+32)],alighn_data0[(k+40)],alighn_data0[(k+48)],alighn_data0[(k+56)]);
      __m128i I15 = _mm_add_epi32 (I6, I7);
      __m128i I16 = _mm_hadd_epi32(I15, I15);
      __m128i I17 = _mm_hadd_epi32(I16, I16);
      // store
      _mm_store_si128(&out_vec[0], I17);
      // _mm_stream_si32(&O[k],out[0]);
      //outpur the result
      O[k] = out[0];

    }


    for (int k=0; k<4; k++)
    {
      __m128i load = _mm_load_si128(&g_aiT16_vec[(k*8)+4]);
      __m128i load1 = _mm_set1_epi16(src[64*(k)+32]);

      // all multiplications
      __m128i load2 = _mm_mullo_epi16(load1, load);
      __m128i load3 = _mm_mulhi_epi16(load1, load);
      __m128i load4 = _mm_unpacklo_epi16(load2, load3);
      //__m128i I5 = _mm_unpackhi_epi16(I2, I3);
      _mm_store_si128(&alighn_data1_vec[k],load4);

    }

    //  printf("%d, %d \n",alighn_data1[4], g_aiT16[6][0]*src[6*16]);

    for (int k=0; k<4; k++)
    {

        EO[k] = alighn_data1[(k)]+alighn_data1[(k+4)]+alighn_data1[(k+8)]+alighn_data1[(k+12)];

    //  EO[k] = g_aiT16[ 2][k]*src[ 2*16] + g_aiT16[ 6][k]*src[ 6*16] + g_aiT16[10][k]*src[10*16] + g_aiT16[14][k]*src[14*16];
    }

         /*Section 3*/
    //Pack into C code and pass it directly to the SIMD
    mulData[0] = g_aiT16[0][0];
    mulData[1] = g_aiT16[0][1];
    mulData[2] = g_aiT16[4][0];
    mulData[3] = g_aiT16[4][1];
    mulData[4] = g_aiT16[8][0];
    mulData[5] = g_aiT16[8][1];
    mulData[6] = g_aiT16[12][0];
    mulData[7] = g_aiT16[12][1];
    mulData[8] = src[0];
    mulData[9] = mulData[8];
    mulData[10] = src[4*16]; 
    mulData[11] = mulData[10];
    mulData[12] = src[8*16];
    mulData[13] = mulData[12];
    mulData[14] = src[12*16];
    mulData[15] = mulData[14];


    temp = _mm_setzero_si128();
    temp2 = _mm_setzero_si128();

    temp = _mm_load_si128(mul0); 
    temp2 = _mm_load_si128(mul1); 

    mul16Low  = _mm_mullo_epi16(temp, temp2);
    mul16high  = _mm_mulhi_epi16(temp, temp2);

    mulTemp = _mm_unpacklo_epi16(mul16Low, mul16high);
    mulTemp1 = _mm_unpackhi_epi16(mul16Low, mul16high);

    mulTemp = _mm_add_epi32(mulTemp, mulTemp1); 

    mulTemp1 = _mm_shuffle_epi32(mulTemp, 0x4E);

    _mm_store_si128(&eeoReg[0], mulTemp1); //debug

    /*printf("orig = EE0 = %d, EE1 = %d, ee2 = %d, EE3 = %d \n", EEO[0], EEO[1], EEO[2], EEO[3]);

    EEO[0] = g_aiT16[4][0]*src[4*16] + g_aiT16[12][0]*src[12*16];
    EEO[1] = g_aiT16[4][1]*src[4*16] + g_aiT16[12][1]*src[12*16];
    EEO[2] = g_aiT16[0][0]*src[0] + g_aiT16[8][0]*src[8*16];
    EEO[3] = g_aiT16[0][1]*src[0] + g_aiT16[8][1]*src[8*16];

    
    printf("Scalar = EE0 = %d, EE1 = %d, EE2 = %d, EE3 = %d \n",EEO[0], EEO[1], EEO[2], EEO[3] );
    */

    /* Combining even and odd terms at each hierarchy levels to calculate the final spatial domain vector */

    /*Section 4*/
    //This entire section can be optimised------
    temp = _mm_setzero_si128();
    temp2 = _mm_setzero_si128();
    el = _mm_setzero_si128();
    eh = _mm_setzero_si128();

    //Load
    eee = _mm_loadl_epi64(eeeReg); 
    eeo = _mm_loadl_epi64(eeoReg); 

    ee = _mm_add_epi32(eee, eeo);       //EE[0] = EEE[0] + EEO[0], EE[1] = EEE[1] + EEO[1]; Ignore high 64
    ee = _mm_insert_epi32 (ee, 0x00, 2); //95:64 = 0x00
    ee = _mm_insert_epi32 (ee, 0x00, 3);//127:96 = 0x00

    //Get rid of the high 64 bytes
    ee = _mm_bslli_si128 (ee, 8); 
    ee = _mm_bsrli_si128 (ee, 8); 

    //32bit word wise subtraction. No borrow
    temp = _mm_sub_epi32(eee, eeo);

    temp2 = temp;

    temp2 = _mm_bslli_si128 (temp2, 12); //Shift ls word to highest word

    temp = _mm_bsrli_si128 (temp, 4); //Eliminate LS word
    temp = _mm_bslli_si128 (temp, 8); //Shift ls word to highest word

    ee = _mm_add_epi32(ee, temp); 
    ee = _mm_add_epi32(ee, temp2); 
    //This entire section can be optimised------

    /*Section 5*/
    eo = _mm_load_si128(eoReg); 
    el = _mm_add_epi32(ee, eo);    //EO[0-3] + EE[0-3]

    //32bit word wise subtraction. No borrow  //E[4] = EE[3] - EO[3]; E[5] = EE[2] - EO[2]; E[6] = EE[1] - EO[1]; E[7] = EE[0] - EO[0];
    temp = _mm_sub_epi32(ee, eo);   
    eh = _mm_shuffle_epi32(temp, 0x1B); //Shuffling internally = mask = 00011011
   
    _mm_store_si128(&elReg[0], el);
    _mm_store_si128(&ehReg[0], eh);


    //Next
    for (int k=0; k<8; k++)
    {
      dst[k]   = MAX( -32768, MIN( 32767, (E[k]   + O[k]   + add)>>shift ));
      dst[k+8] = MAX( -32768, MIN( 32767, (E[7-k] - O[7-k] + add)>>shift ));
    }
    src ++;
    dst += 16;
  }
}


static void idct16_scalar(short* pCoeff, short* pDst)
{
  short tmp[ 16*16] __attribute__((aligned(16)));
  partialButterflyInverse16(pCoeff, tmp, 7);
  partialButterflyInverse16(tmp, pDst, 12);
}

/// CURRENTLY SAME CODE AS SCALAR !!
/// REPLACE HERE WITH SSE intrinsics
static void idct16_simd(short* pCoeff, short* pDst)
{
  short tmp[ 16*16] __attribute__((aligned(16)));
  partialButterflyInverse16_simd(pCoeff, tmp, 7);
  partialButterflyInverse16_simd(tmp, pDst, 12);
}

int main(int argc, char **argv)
{
    //allocate memory 16-byte aligned
    short *scalar_input = (short*) memalign(16, IDCT_SIZE*IDCT_SIZE*sizeof(short));
    short *scalar_output = (short *) memalign(16, IDCT_SIZE*IDCT_SIZE*sizeof(short));

    short *simd_input = (short*) memalign(16, IDCT_SIZE*IDCT_SIZE*sizeof(short));
    short *simd_output = (short *) memalign(16, IDCT_SIZE*IDCT_SIZE*sizeof(short));

    //initialize input
    printf("input array:\n");
    for(int j=0;j<IDCT_SIZE;j++){
        for(int i=0;i<IDCT_SIZE;i++){
            short value = rand()%2 ? (rand()%32768) : -(rand()%32768) ;
            scalar_input[j*IDCT_SIZE+i] = value;
            simd_input  [j*IDCT_SIZE+i] = value;
      printf("%d\t", value);
        }
        printf("\n");
    }

    idct16_scalar(scalar_input, scalar_output);
    idct16_simd  (simd_input  , simd_output);

    //check for correctness
    compare_results (scalar_output, simd_output, "scalar and simd");

    printf("output array:\n");
    for(int j=0;j<IDCT_SIZE;j++){
        for(int i=0;i<IDCT_SIZE;i++){
      printf("%d\t", scalar_output[j*IDCT_SIZE+i]);
        }
        printf("\n");
    }

    //Measure the performance of each kernel
    benchmark (idct16_scalar, scalar_input, scalar_output, "scalar");
    benchmark (idct16_simd, simd_input, simd_output, "simd");

    //cleanup
    free(scalar_input);    free(scalar_output);
    free(simd_input); free(simd_output);
}

