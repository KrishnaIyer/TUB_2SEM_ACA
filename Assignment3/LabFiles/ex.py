import subprocess, os, re, time

#Global
key = "script.py"
inputdir = "benchmarks"
outputdir = "Output"
i = 0
j = 0

#-------------------------------Part 1--------------------------------------------------------------------
print "Executing Part 1"

#List
otherArguments =' -fetch:ifqsize 1 -decode:width 1 -issue:width 1 -commit:width 1 -issue:inorder true -res:ialu 1 -res:imult 1 -res:fpalu 1 -res:fpmult 1'
mainArgument = ['-bpred nottaken','-bpred bimod','-bpred 2lev','-bpred comb','-bpred perfect']
subdir = outputdir + '/' "Part1"
sim_IPC = ''
IPC = ''
l = []

#This part runs the command and places stuff inside a specific file with a time stamp
for fichier in os.listdir(inputdir):
    print 'Executing :' + fichier
    date = 'date >'+ subdir +'/'+fichier+'.txt'	#Place a timestamp
    subprocess.call(date, shell=True)
    l.append(fichier)
    l.append(' : ')

    for arg in mainArgument: 
    	command = 'script -qc "./sim-outorder '+ arg + otherArguments+' ' +inputdir+'/'+fichier+'">>'+ subdir +'/'+fichier+'.txt'
    	subprocess.call(command, shell=True)	
    		    #Extract the SIM_IPC value and store in a special file

#write to final output
del l[:]
del sim_IPC
for fichier in os.listdir(subdir):
	l.append(fichier)
	l.append(' :     ')

	fichier = os.path.join("Output/Part1", fichier)
	f = open(fichier, 'r')
	for line in f:
		if(line.find("sim_IPC") == 0):
			temp = ''
			temp = re.findall(r"\d*\.\d+|\d+", line)
			IPC =  ''.join(temp)
			l.append(IPC)
			l.append(':')
			IPC = ''
	f.closed

	l.append('\n')
	sim_IPC = ''.join(l)
print sim_IPC

text_file = open(outputdir + "/Consolidated.txt", "w")
text_file.write("Results \n------------------------------------------------\n")
text_file.write("Part 1:\n")
text_file.write("Settings: %s \n" %otherArguments)	
text_file.write("Branch Prediction Mode: %s \n" %mainArgument)
text_file.write(sim_IPC)
text_file.write("\n\n")
text_file.close()


#remove the unnecessary typescript file that gets generated
subprocess.call("rm typescript", shell=True)


