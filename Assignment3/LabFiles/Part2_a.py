import subprocess, os, re, time

#Global
inputdir = "benchmarks"
outputdir = "Output"
i = 0
j = 0

#-------------------------------Part 1--------------------------------------------------------------------
print "Executing Part 2: In order"

#List
otherArguments =' -issue:inorder true -bpred 2lev'
mainArgument = \
['-fetch:ifqsize 1 -decode:width 1 -issue:width 1 -commit:width 1 -res:ialu 1 -res:imult 1 -res:fpalu 1 -res:fpmult 1',\
 '-fetch:ifqsize 2 -decode:width 2 -issue:width 2 -commit:width 2 -res:ialu 2 -res:imult 2 -res:fpalu 2 -res:fpmult 2',\
 '-fetch:ifqsize 4 -decode:width 4 -issue:width 4 -commit:width 4 -res:ialu 4 -res:imult 4 -res:fpalu 4 -res:fpmult 4',\
 '-fetch:ifqsize 8 -decode:width 8 -issue:width 8 -commit:width 8 -res:ialu 8 -res:imult 8 -res:fpalu 8 -res:fpmult 8',\
 '-fetch:ifqsize 8 -decode:width 8 -issue:width 8 -commit:width 8 -res:ialu 4 -res:imult 1 -res:fpalu 4 -res:fpmult 1']
subdir = outputdir + '/' "Part2_a"
sim_IPC = ''
IPC = ''
l = []

#This part runs the command and places stuff inside a specific file with a time stamp
for fichier in os.listdir(inputdir):
    print 'Executing :' + fichier
    date = 'date >'+ subdir +'/'+fichier+'.txt'	#Place a timestamp
    subprocess.call(date, shell=True)
    l.append(fichier)
    l.append(' : ')

    for arg in mainArgument: 
    	command = 'script -qc "./sim-outorder '+ arg + otherArguments+' ' +inputdir+'/'+fichier+'">>'+ subdir +'/'+fichier+'.txt'
    	subprocess.call(command, shell=True)	
    		    #Extract the SIM_IPC value and store in a special file

#write to final output
del l[:]
del sim_IPC
for fichier in os.listdir(subdir):
	l.append(fichier)
	l.append(' : \t\t\t\t')

	fichier = os.path.join("Output/Part2_a", fichier)
	f = open(fichier, 'r')
	for line in f:
		if(line.find("sim_IPC") == 0):
			temp = ''
			temp = re.findall(r"\d*\.\d+|\d+", line)
			IPC =  ''.join(temp)
			l.append(IPC)
			l.append(':')
			IPC = ''
	f.closed

	l.append('\n')
	sim_IPC = ''.join(l)
print sim_IPC

text_file = open(outputdir + "/Consolidated.txt", "a")
text_file.write("\n------------------------------------------------\n")
text_file.write("Part 2.a: In Order:\n")
text_file.write("Settings: %s \n" %otherArguments)
text_file.write("Main Argument for execution: \n")	
for arg in mainArgument:
	text_file.write("%s\n" %arg)
text_file.write(sim_IPC)
text_file.write("\n\n")
text_file.close()


#remove the unnecessary typescript file that gets generated
subprocess.call("rm typescript", shell=True)


