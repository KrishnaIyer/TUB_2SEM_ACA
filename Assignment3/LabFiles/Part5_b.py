import subprocess, os, re, time

#Global
inputdir = "benchmarks"
outputdir = "Output"
fichier  = "memcopy"
i = 0
j = 0

#-------------------------------Part 1--------------------------------------------------------------------
print "Executing Part 5.b: Choosing optimal cache configuration"

#List
otherArguments =' -mem:lat 200 2 -ruu:size 32 -lsq:size 16 -fetch:ifqsize 4 -decode:width 4 -issue:width 4 -commit:width 4 -issue:inorder false -bpred comb'
mainArgument = ['-cache:dl2 ul2:1024:64:2:l -cache:dl2lat 6','-cache:dl2 ul2:2048:128:4:l -cache:dl2lat 8' , '-cache:dl2 ul2:4096:256:8:l -cache:dl2lat 10' , '-cache:dl2 ul2:8192:512:16:l -cache:dl2lat 12', '-cache:dl2 ul2:16384:1024:32:l -cache:dl2lat 14', '-cache:dl2 ul2:32768:2048:64:l -cache:dl2lat 16','-cache:dl2 ul2:65536:4096:128:l -cache:dl2lat 18']
subdir = outputdir + '/' "Part5_b"
sim_IPC = ''
IPC = ''
l = []

#This part runs the command and places stuff inside a specific file with a time stamp
print 'Executing :' + fichier
date = 'date >'+ subdir +'/'+fichier+'.txt'	#Place a timestamp
subprocess.call(date, shell=True)
l.append(fichier)
l.append(' : ')

for arg in mainArgument: 
	command = 'script -qc "./sim-outorder '+ arg + otherArguments+' ' +inputdir+'/'+fichier+'">>'+ subdir +'/'+fichier+'.txt'
	subprocess.call(command, shell=True)	
    		    #Extract the SIM_IPC value and store in a special file

#write to final output
del l[:]
del sim_IPC
for fichier in os.listdir(subdir):
	l.append(fichier)
	l.append(' : \t\t\t\t')

	fichier = os.path.join("Output/Part5_b", fichier)
	f = open(fichier, 'r')
	for line in f:
		if(line.find("sim_IPC") == 0):
			temp = ''
			temp = re.findall(r"\d*\.\d+|\d+", line)
			IPC =  ''.join(temp)
			l.append(IPC)
			l.append(':')
			IPC = ''
	f.closed

	l.append('\n')
	sim_IPC = ''.join(l)
print sim_IPC

text_file = open(outputdir + "/Consolidated.txt", "a")
text_file.write("\n------------------------------------------------\n")
text_file.write("Part 5.b: Choosing optimal cache configuration\n")
text_file.write("Settings: %s \n" %otherArguments)	
text_file.write("Main Argument for execution: \n")	
for arg in mainArgument:
	text_file.write("%s\n" %arg)
text_file.write(sim_IPC)
text_file.write("\n\n")
text_file.close()


#remove the unnecessary typescript file that gets generated
subprocess.call("rm typescript", shell=True)


