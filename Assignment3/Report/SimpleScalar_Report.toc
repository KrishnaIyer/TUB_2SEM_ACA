\contentsline {section}{\numberline {1}Findings}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}Branch Prediction}{3}{subsection.1.1}
\contentsline {subsection}{\numberline {1.2}Processor Width}{4}{subsection.1.2}
\contentsline {subsection}{\numberline {1.3}Ruu/Lsq}{6}{subsection.1.3}
\contentsline {subsection}{\numberline {1.4}A Report on Experiments 1-3}{7}{subsection.1.4}
\contentsline {subsubsection}{\numberline {1.4.1}Factor/s limiting IPC initially}{7}{subsubsection.1.4.1}
\contentsline {subsubsection}{\numberline {1.4.2}Effect of Branch Prediction on IPC}{7}{subsubsection.1.4.2}
\contentsline {subsubsection}{\numberline {1.4.3}Effect of Processor Width}{7}{subsubsection.1.4.3}
\contentsline {subsubsection}{\numberline {1.4.4}Effect of ruu,lsq}{7}{subsubsection.1.4.4}
\contentsline {subsubsection}{\numberline {1.4.5}Why IPC is hitting a wall with benchmarks that have the lowest improvements?}{8}{subsubsection.1.4.5}
\contentsline {subsection}{\numberline {1.5}Cache Architecture Proposal}{8}{subsection.1.5}
\contentsline {subsubsection}{\numberline {1.5.1}AMAT Calculation for Latencies}{8}{subsubsection.1.5.1}
\contentsline {subsubsection}{\numberline {1.5.2}Cache Architecture Proposal}{8}{subsubsection.1.5.2}
