
; -----------------------------------------------------------------------
; Advanced Computer Architecture Lab
; TU Berlin (AES)
; Group 2
; Krishna Iyer Easwaran
; Amar Surulu
; Brief:
; MIPS 64 program for Smooth.c which is some sort of a filtering program
;-------------------------------------------------------------------------




; Data definitions
		.data
 N_COEFFS:  	.word 		3
 N_SAMPLES:		.word		5
 coeff:			.double		0.5 ,1.0 ,0.5
 sample:		.double		1.0 ,2.0 ,1.0 ,2.0 ,1.0
 result:		.double		0.0 ,0.0 ,0.0 ,0.0 ,0.0




;Code

		.text
;First load the values onto the floating point registers using the reg addressing format
		L.D   F0,sample(R0)     	;F0 = sample
    	L.D   F2,coeff(R0)    		;F2= coeff
    	LD 	  R11, N_COEFFS(R0)		;R11 = N_COEFFS



Loop: 	MUL.D 	F6, F0, F2			;Need to deal with overflow
		ADD.D 	F7 , F6, 0  		;accumulate
		DSUB	R11, 1, R11			;Decrement
		BNEZ    R11, Loop  			;Loop jump check
 


