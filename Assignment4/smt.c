/*
* Team: 
*------------------
* Krishna Iyer Easwaran
* Borja Revuelta Fernandez
* Amar Suluru
*
*/

#include <unistd.h>
#include <pthread.h>
#include <sched.h>
#include <sys/syscall.h>
#include <errno.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <complex.h> 

//define the number here with the core assigned to you
#define CPUID_SMT0 0
#define CPUID_SMT1 (CPUID_SMT0+6)

//enable and disable your SMT thread
#define SMT1_ON 1

#define VAR_SIZE 1001

/*
* Note on Sandy Bridge:
*===========================================================================================
* The intel sandy bridge architecture has the following special features
* a. 3 Execution Units
* ---------------------
* The Sandy Bridge has 3 execution ports/units Port 0 , Port 1 and Port 5. 
* These ports share the Integer GPRs and SIMD regs. 
* Port 0 -> FP MUL, BLEND and DIV 
* Port 1 -> FP ADD
* Port 5 -> FP Shuffle, Blend, FP Bool
* The Out of Order instruction scheduler deals with "scheduling" instructions to these ports.
* 
* b. Branch Prediction:
* ---------------------
* It uses a 1-bit predictor with high accuracy. The Branch Target Buffer(BTB) is shortened
* and the remaining space is filled with a bigger global branch history to improve accuracy.
* 
*
*/


volatile int b[VAR_SIZE] = {25};
/*
* Disassembly Info:
* =======================================================================================
* When this code is disassmbled, the following resources are used by it.
* - All Registers used by Fibonacci are integer regs. 
* - This code has 8 branch instruction (je. jne, jmp)
* - This causes it to use the BTB and the branch predictor
*/
static int fibonacci(int n){
  if(n==0){
    return 0;
  }
  else{
    if(n==1)
      return 1;
    else
      return (fibonacci(n-1)+fibonacci(n-2));
  }
}



static void *tf_smt0(void *targ){
  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(CPUID_SMT0, &mask);
  pid_t tid = syscall(SYS_gettid); //glibc does not provide a wrapper for gettid
  sched_setaffinity(tid, sizeof(cpu_set_t), &mask);

  printf("Fibonacci number 40 = %d\n", fibonacci(40));

  pthread_exit(NULL);
}


/*
* Final Stats(Average of 15 runs):
* ================================
* Single Thread Execution 		: 	500ms
* Two threads with empty loop	:   725ms
* Two threads with our code  	:   550ms
* Average improvement			:   22-25%
*
*/

/*
* How is the speed up achieved:
*------------------------------
* In the Original code with the while loop, the while loop gets compiled to the following:
* - 400a57: jmp    400a57 <tf_smt1+0x47> '; basically a jump to itself.
* This instruction gets filled in the BTB and stalls the branch prediction unit,
* since the Fibonacci code also uses Branch Prediction.
* When our FP code is added, the branch prediction condition is reached much slower because 
* of the complex FP operations performed.
* Hence, the Fibonacci thread can run with less interruption. 
*
*/

static void *tf_smt1(void *targ){

  cpu_set_t mask;
  CPU_ZERO(&mask);
  CPU_SET(CPUID_SMT1, &mask);
  pid_t tid = syscall(SYS_gettid); //glibc does not provide a wrapper for gettid
  sched_setaffinity(tid, sizeof(cpu_set_t), &mask);

  while (1){
    // put your "background" code here
    // no sleeps are other code that schedules the thread out is allowed
    // inline assembly is not allowed
    volatile int i, j;
    double *a, *c;
    volatile double mat[VAR_SIZE][VAR_SIZE];

    //Allocate data on the heap
    a = malloc(VAR_SIZE);
    c = malloc(VAR_SIZE);

    //Initialise the variables with floating point values
    for(i=1; i<VAR_SIZE; i++)
    {
      a[i] = i * 10.5;
      b[i] = i +10.09;
      c[i] = i;
    }

    //Shift using PI to get a wide range of initial values 
    for(i=1; i<VAR_SIZE; i++)
    {
      a[i] = (i * M_PI);
      b[i] = (i * M_PI * 10.09);
      c[i] = M_PI;
    }

    // Shuffle between the registers to waste some time of the FP ops.
    for(i=1; i<VAR_SIZE; i++)
    {
      a[i] = (c[i] * M_PI);
      b[i] = (a[i] * M_PI * 10.09);
      c[i] = b[i];
    }

   //Use column major loop traversal to make this thread slow.
    for(i = 0; i <VAR_SIZE; i++ )
    {
      for(j =0; j < VAR_SIZE; j++)
      {
        mat[j][i] = b[j] + c[j] + a[j]; 
 
        if(mat[j][i] < mat[j][i])
        {
          mat[i][j] = mat[j][i];
        }
        else
        {
          mat[j][i] = mat[i][j];
        }
      }
    }

    //Write back result
    memcpy(a,c, VAR_SIZE);


    //To be sure that the previous loop is not optimized away by the compiler
    printf("This should not appear\n");
    pthread_exit(NULL);
  }

}
static int64_t diff(struct timespec start, struct timespec end)
{
  struct timespec temp;
  int64_t d;
  if ((end.tv_nsec-start.tv_nsec)<0) {
      temp.tv_sec = end.tv_sec-start.tv_sec-1;
      temp.tv_nsec = 1000000000+end.tv_nsec-start.tv_nsec;
  } else {
      temp.tv_sec = end.tv_sec-start.tv_sec;
      temp.tv_nsec = end.tv_nsec-start.tv_nsec;
  }
  d = temp.tv_sec*1000000000+temp.tv_nsec;
  return d;
}

int main ()
{
  pthread_t smt0, smt1;
  struct timespec start, end;

  clock_gettime(CLOCK_REALTIME,&start);

  pthread_create(&smt0, NULL, tf_smt0, NULL);
#if SMT1_ON
  pthread_create(&smt1, NULL, tf_smt1, NULL);
#endif

  pthread_join(smt0, NULL);
  clock_gettime(CLOCK_REALTIME,&end);
#if SMT1_ON
  pthread_cancel(smt1);
#endif

  double time = (double) diff(start,end)/1000000;
  printf("\nExecution time thread SMT0 \t %.3f ms\n", time);

  return 0;
}

/*
Copy local to remote:  scp -P 20222 ~/Documents/ACA_Lab/Assignment4/smt.c amarsuluru@lianli.aes.tu-berlin.de:~/smt/smt.c 
*/