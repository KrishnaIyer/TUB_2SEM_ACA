ACA Assignment 4 : SMT
======================
In the final exercise you will investigate the effect of executing two threads simultaneously on the
same physical core capable of Simultaneous Multithreading (SMT). SMT allows better utilization of
core resources by selecting independent instruction from multiple threads simultaneously. The effec-
tiveness, however, varies depending on the workload the threads are performing. With SMT most core
resources are shared (e.g. functional units, reservation stations, reorder buffers, branch predictor ent-
ries, etc), only the architectural register files are duplicated. Because the core resources are shared,
when threads content for the same resources performance of both threads reduce.


Steps to be followed
====================
1. Read the sandy bridge architecture and understand the scheduler and resources.
	Datasheet(Intel i7-3xxx)
	https://www.intel.com/content/www/us/en/processors/core/3rd-gen-core-desktop-vol-1-datasheet.html
	https://www.intel.com/content/www/us/en/processors/core/3rd-gen-core-desktop-vol-2-datasheet.html

2. Disassembe the original source code in the server and then understand which resources(registers) are used.
3. Try to write a program to use different resources so that the first thread is faster.
4. Write a detailed report on the above.
5. Target: 20% speed up with a detailed report.


TODO:
====
1. Set up Repo 				: Done
2. Create report template   : Done
3. Study Architecture       : Done
4. Understand fibonacci.    : Not Started
5. Write initial code 		: Done
6. Try speed up				: Done
7. Write Report(in code)	: Not started

Bugs:
====

Useful Commands:
================
1. Copy from local to remote: 
- $scp -P 20222 PATH/smt.c amarsuluru@lianli.aes.tu-berlin.de:~/smt_mod/smt.c 

2. Connect to remote:
- $ssh -p 20222 amarsuluru@lianli.aes.tu-berlin.de

3. Disassemble on the server:
- $make
- $objdump -D smt
